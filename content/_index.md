# Bienvenue sur mon blog

Je m'appelle Raj Porus Hiruthayaraj, et je suis actuellement un étudiant en informatique qui va entamer son cycle d'ingénieur en alternance à L'ESIEE Paris.

## Ma méthode de travail

J'ai longtemps changé d'éditeurs de texte, un bon éditeur de texte est essentiel pour un développeur.
Ce dernier consacrant une grande partie de son travail dans cet environnement, il vaut mieux que son éditeur soit agréable à utiliser.
J'ai pendant longtemps utilisé Vim et Neovim, mais j'ai arrêté de changer d'éditeur lorsque j'ai découvert Emacs.
C'est un éditeur très extensible et programmable à souhait.

{{< picture "emacs.png" "emacs.png" "Image alt text" >}}

Un de mes camarades parle de moi sur son blog, je vous laisse lire son [article](https://etoile.netlify.app/mon-histoire-avec-linux-rencontre/).

## Mes passions

Je suis passionné par le minimalisme informatique et les logiciels libres, j'aimerais un jour travailler en tant que mainteneur du Kernel Linux.
J'utilise GNU/Linux au quotidien et je personnalise jusque dans les moindre détails mon système et je partage cela avec les autres utilisateurs de la communauté :
- [Fichiers config](https://github.com/RajPorus19/ConfigFiles)

Ma contribution à l'open source ne s'arrête pas là, j'aide aussi activement plusieurs projets en créant des issues, et lorsque ma connaissance me le permet, il m'arrive d'en résoudre certains.

{{< picture "issues.png" "issues.png" "Image alt text" >}}
{{< picture "neovide.png" "neovide.png" "Image alt text" >}}

De plus, je pratique durant mon temps libre la course d'endurance, elle me permet de méditer et de m'éloigner du code, grâce à cela je prends du recul pour mieux coder par la suite.
