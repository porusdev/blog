+++
aliases = ["posts", "articles", "blog", "showcase", "docs","stage","stages"]
title = "Stages"
author = "Raj Porus Hiruthayaraj"
tags = ["index"]
weight = 1
+++

Dans cette rubrique, vous trouverez des articles qui traitent de mes différents stages réalisé dans le cadre de mon BTS SIO option SLAM. Ces stages m’ont permis d’avoir une première expérience dans le monde professionnel de la programmation. De plus, le dernier stage en date m’a permis de décrocher une alternance pour le cycle d’ingénieur que je vais effectuer chez ESIEE Paris.
