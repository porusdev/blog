+++
author = "Raj Porus Hiruthayaraj"
title = "Mon stage chez Markus"
tags = [
"internship",
]
weight = 200
thumbnail = "images/markus-thumbnail.png"
+++



{{< picture "markus-thumbnail.png" "markus-thumbnail.png" "Image alt text" >}}

## Comment ai-je trouvé ce stage ?

J'ai trouvé ce stage grâce à la plateforme de recrutement Indeed. Après y avoir déposé mon CV, j'ai reçu
un message de la part de monsieur Laurent Goulenok, Fondateur de CEOS Tech, cette start-up est derrière
le projet Markus. S'en est suivi un entretien qui devait être en vidéoconférence, mais qui au final était un appel téléphonique,
en raisons de soucis techniques. J'ai eu alors l'occasion de me présenter et de comprendre les raisons de son intérêt pour
ma candidature. L'entretien s'est bien passé, j'ai transmis sans perdre de temps ma convention de stage.

## En quoi consiste Markus ?

Markus est une application mobile qui sera présente sous IOS et Android. Cette application se veut être "le bras droit des
restaurateurs", car ce dernier va proposer aux personnes qui sont à la tête d'un restaurant, de gérer plusieurs activités
de leur quotidien dans une seule et même application. Il est important de préciser cela, car c'est la raison d'être de ce projet.

Monsieur Laurent Goulenok est un ancien restaurateur qui as conçu ce projet en se basant sur les difficultés qu'il a rencontrées.
La première problématique était que le restaurateur se retrouve à utiliser plusieurs outils par tâches donc paie le prix pour
chacun de ses outils et a parfois du mal à s'y retrouver.

La seconde, est le fait que les restaurateurs ont tendance à ne pas accorder à leurs données, l'attention et le traitement qu'ils méritent. En effet dans sa version finale, Markus a pour but d'utiliser
l'intelligence artificielle entraîné par les données du restaurant de l'utilisateur pour fournir à chaque restaurateur, les
prévisions de ventes les plus précises possible.

## Ma contribution

L'application Markus communique avec une API Rest écris en Python en utilisant le framework Django Rest Framework.
Ma candidature avait intéressé cette start-up, car leur développeurs ont rencontré une grande difficultés à comprendre 
et à utiliser ce dernier. C'est ici que j'interviens, car j'avais appris en autodidacte ce framework.

En ce sens, ma première mission a été de former l'équipe de développeur à mieux comprendre le framework, afin que ces
derniers puissent continuer le développement sur l'application front-end.
Dans un second temps, j'ai fais part à l'équipe des problèmes présents sur leur back-end.
Il y avait de nombreux soucis, comme une mauvaise séparation des responsabilités, l'absence d'authentification et de cloisonnement, et de nombreuses fonctions qui n'était pas nécessaires, car elles étaient déjà prise en charge par le framework.

## A1.1.1 Analyse du cahier des charges d'un service à produire
C'est ce diagramme qui m'a en partie aidé à mieux saisir les services attendus de la part du back-end sur lequel je travaillais.

{{< picture "markus-diagramme.jpg" "markus-diagramme.jpg" "Image alt text" >}}

Mes missions :
- Venir en aide à l'équipe de développeur sur le framework Django
- Harmoniser le front-end et le back-end
- Corriger les bugs d'authentifications
- Ajouter les fonctionnalisées nouvelles (v1)
## A1.1.2 Étude de l'impact de l'intégration d'un service sur le système

L'intégration du service JWT, nous permettait de répondre à plusieurs attentes de notre projet,
tout d'abord l'authentification, et d'une autre part l'identification, car une clé JWT possède
en son sein, l'identifiant associé à l'utilisateur, Ainsi, il était plus simple de charger les
informations concernant cet utilisateur dans l'application. Pour intégrer JWT on télécharge la librairie python associé avec le gestionnaire de paquets PIP et ensuite, on ajoute la ligne "DEFAULT AUTHENTICATION CLASSES" à cet endroit dans le fichier "settings.py" du projet :

```
REST_FRAMEWORK = {
...
    'DEFAULT_AUTHENTICATION_CLASSES': ('rest_framework_simplejwt.authentication.JWTAuthentication',),
...
}
```
Cette ligne fait de JWT le moyen d'authentification par défaut sur toutes les vues de notre API.
## A1.2.3 Évaluation des risques liés à l'utilisation d'un service
Avant de déployer le projet sur la plateforme Heroku, j'ai testé le fichier "requirements.txt"
dans un environnement virtuel pour être sûr que toutes les dépendances du projet y sont inclus, et ajouté ceux qui ne l'étaient pas :
```
asgiref==3.3.1
cffi==1.14.4
cryptography==3.3.1
dj-database-url==0.5.0
Django==3.1.5
django-filter==2.4.0
django-heroku==0.3.1
django-on-heroku==1.1.2
django-rest-knox==4.1.0
django-rest-passwordreset==1.1.0
djangorestframework==3.12.2
djangorestframework-simplejwt==4.6.0
gunicorn==20.0.4
Markdown==3.3.3
Pillow==8.1.0
psycopg2==2.8.6
psycopg2-binary==2.8.6
pycparser==2.20
PyJWT==2.0.1
pytz==2020.5
six==1.15.0
sqlparse==0.4.1
whitenoise==5.2.0
```
## A1.2.4   Détermination des tests nécessaires à la validation d'un service  
Une application en parralèle de Markus était réalisé par une autre équipe que j'ai pu aider avec les tests unitaires : 
```
[19:25]Corentin:
Salut, j'ai un probleme avec le test de relation entre ingredient et produit je voulais savoir si tu savais comment regler ca je te met le code la
[19:27]Corentin:

class Produit(models.Model):
    nom = models.CharField('nom', max_length=200)
    description = models.CharField('description', max_length=500, null = True)
    categorie = models.CharField('categorie', max_length=100)
    prix = models.CharField('prix', max_length=9)    
    panier = models.ForeignKey(Panier, on_delete = models.CASCADE, null=True)
    ingredients = models.ManyToManyField(Ingredients)


ca c'est la table produit
[19:27]
Corentin:
et ca le code qui beug
[19:27]Corentin:

    def test_produit3(self):
        ingredient1 = Ingredients.objects.create(nom_ingredient="pain", description_ingr="pain pour emburger", prix_ingredient=1,categorie_ingr="pain")
        ingredient2 = Ingredients.objects.create(nom_ingredient="steak", description_ingr="steak de boeuf", prix_ingredient=1,categorie_ingr="viande")
        burger= Produit.objects.create(nom = "burger",description="burger fait maison", categorie="formule", prix=2, ingredients= ingredient1)

[19:28]Corentin:
et l'erreur:  TypeError: Direct assignment to the forward side of a many-to-many set is prohibited. Use ingredients.set() instead.

[20:06] Raj Porus: 
Non tu peux pas instancier avec un modèle many to many
[20:07] Raj Porus:
Crée le sans ingrédient puis tu ajoute avec Produit.ingredients.add() ou Produit.ingredients.set()
[20:28]Corentin:
Ah nice, ça marche !
```

## A1.3.4 Déploiement d'un service
Après avoir vérifié le fichier "requirements.txt" le déploient s'est déroulé sans soucis :
https://markus-app.herokuapp.com/
## A1.4.1 Participation à un projet
Le suivi du projet à été orchestré depuis la plateforme Trello :

{{< picture "markus-trello.png" "markus-trello.png" "Capture écran du Trello de Markus" >}}

## A2.2.1 Suivi et résolution d'incidents
J'ai résolu les problèmes que l'équipe a rencontrés avec Django et git, et je les ai formés là-dessus, je n'ai pas de preuve sous forme de document, car tout cela a été fait travers des appels sur google meets.
Cependant voici une retranscription d'un bug type :
```
Nom du bug : conflits de dépendances avec les packages node
Personne à l'origine : Hajar Ouhmeri
Solution du problème : Mettre en gitignore le dossier avec les packages et laisser seulement le fichier qui indique les dépendances à importer.
```

## A2.3.1 Identification, qualification et évaluation d'un problème
Markus utilise certaines dépendances dépréciées et j'ai donc expliqué à mon tuteur les risques de l'utilisation de ces dernières mais j'ai conclu en lui disant que changer ces dépendances n'étaient pas une priorité car il n'y a pas de menaces de sécurité à ce niveau pour le moment, cela voulait seulement dire que cette dépendance n'est plus supportée par ses développeurs.
```
{
  "name": "ektor",
  "version": "0.0.1",
  "private": true,
  "scripts": {
    "android": "react-native run-android",
    "ios": "react-native run-ios",
    "start": "react-native start",
    "test": "jest",
    "lint": "eslint .",
    "react-devtools": "react-devtools"
  },
  "dependencies": {
    "@fortawesome/fontawesome-svg-core": "^1.2.30",
    "@fortawesome/free-solid-svg-icons": "^5.14.0",
    "@fortawesome/react-native-fontawesome": "^0.2.5",
    "@iconify/icons-bi": "^1.0.6",
    "@iconify/react": "^1.1.3",
    "@react-native-community/async-storage": "^1.11.0",
    "@react-native-community/checkbox": "^0.5.6",
    "@react-native-community/masked-view": "^0.1.10",
    "@react-navigation/bottom-tabs": "^5.6.1",
    "@react-navigation/drawer": "^5.8.4",
    "@react-navigation/native": "^5.6.1",
    "@react-navigation/stack": "^5.6.2",
    "axios": "^0.21.1",
    "gradle": "^1.2.3",
    "js-base64": "^2.6.2",
    "moment": "^2.27.0",
    "moment-timezone": "^0.5.31",
    "prop-types": "^15.7.2",
    "random-string": "^0.2.0",
    "react": "16.11.0",
    "react-dev-tools": "0.0.1",
    "react-dom": "^16.13.1",
    "react-native": "^0.62.2",
    "react-native-animated-loader": "0.0.8",
    "react-native-calendar-strip": "^2.0.6",
    "react-native-calendars": "^1.300.0",
    "react-native-checkbox-form": "^1.1.5",
    "react-native-datepicker": "^1.7.2",
    "react-native-document-picker": "^3.5.3",
    "react-native-dropdown-picker": "^3.0.5",
    "react-native-email": "^1.1.0",
    "react-native-file-manager": "^1.1.1",
    "react-native-gesture-handler": "^1.9.0",
    "react-native-html-to-pdf": "^0.8.0",
    "react-native-image-picker": "^2.3.3",
    "react-native-linear-gradient": "^2.5.6",
    "react-native-material-dropdown": "^0.11.1",
    "react-native-modalbox": "^2.0.0",
    "react-native-pdf": "^6.2.0",
    "react-native-phone-call": "^1.0.9",
    "react-native-phone-input": "^0.2.4",
    "react-native-picker-select": "^7.0.0",
    "react-native-progress": "^4.1.2",
    "react-native-reanimated": "^1.9.0",
    "react-native-safe-area-context": "^3.0.7",
    "react-native-screens": "^2.9.0",
    "react-native-select-multiple": "^2.1.0",
    "react-native-shapes": "^0.1.0",
    "react-native-share": "^3.7.0",
    "react-native-signature-capture": "^0.4.10",
    "react-native-svg": "^12.1.0",
    "react-native-svg-charts": "^5.4.0",
    "react-native-swipe-gestures": "^1.0.5",
    "react-native-swipeout": "^2.3.6",
    "react-native-vector-icons": "^6.6.0",
    "react-native-view-pdf": "^0.11.0",
    "react-redux": "^7.2.1",
    "redux": "^4.0.5"
  },
  "devDependencies": {
    "@babel/core": "^7.6.2",
    "@babel/runtime": "^7.6.2",
    "@react-native-community/eslint-config": "^0.0.5",
    "babel-jest": "^24.9.0",
    "eslint": "^6.5.1",
    "jest": "^24.9.0",
    "metro-react-native-babel-preset": "^0.58.0",
    "react-test-renderer": "16.11.0",
    "redux-devtools": "^3.5.0"
  },
  "jest": {
    "preset": "react-native"
  }
}
```
## A2.3.2 Proposition d'amélioration d'un service
Toutes les entrées de marchandises sont rentrées une première fois à la main par le restaurateur, j'ai donc proposé un parseur de fichier EXCEL pour que cette tâche aille plus vite.
Ma proposition a été retenue, mais cela n'a pas encore été codé.
## A4.1.3 Conception ou adaptation d'une base de données
Un produit peut avoir plusieurs fournisseurs et un fournisseur peut livrer plusieurs produits, c'est une modification que j'ai apporté lors de la conception de la base de données, cela se retranscrit dans le framework par le mot clé "ManyToManyField".
```
class ProduitEnStock(models.Model):
    ...

   restaurant = models.ForeignKey(
        Restaurant,
        on_delete=models.CASCADE)

    fournisseur = models.ManyToManyField(Fournisseur)
```
## A4.1.9   Rédaction d'une documentation technique
En guise de documentation technique j'ai réalisé une collections de requêtes essentielles de l'application :
```
{
  "info": {
    "_postman_id": "aaa82605-2905-44cb-a349-17741d350a44",
    "name": "test",
    "schema": "https://schema.getpostman.com/json/collection/v2.0.0/collection.json"
  },
  "item": [
    {
      "name": "Login",
      "id": "8155d68d-1245-4d55-bab3-bb1b3e6d04f9",
      "request": {
        "auth": {
          "type": "bearer",
          "bearer": {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjEwNTM1MzMwLCJqdGkiOiJmMDE4MDdlZjEyOGI0MTBkYjcxYWYzMDk2MTQ0MGY0MSIsInVzZXJfaWQiOjR9.dKsqknd15vXifE8eM1oK8kM6LYEJ4M5UZmNLsQcOaVU"
          }
        },
        "method": "POST",
        "header": [],
        "body": {
          "mode": "urlencoded",
          "urlencoded": [
            {
              "key": "username",
              "value": "register@gmail.com",
              "type": "text"
            },
            {
              "key": "password",
              "value": "newmdp123",
              "type": "text"
            }
          ]
        },
        "url": "http://127.0.0.1:8000/accounts/token/",
        "description": "Login"
      },
      "response": []
    },
    {
      "name": "Create personnel",
      "id": "e42a522a-3d25-47e4-95dc-355c7655145f",
      "request": {
        "auth": {
          "type": "bearer",
          "bearer": {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjEwNTM1NTk2LCJqdGkiOiIzMWY1MmMwOTZjYjg0YWFhOTY0NTAwYzBhYjZlYzY3OCIsInVzZXJfaWQiOjR9.uU7u1nvcv-k0GUhRCq5c22WCpUneloqBIQn9QOnKrWg"
          }
        },
        "method": "POST",
        "header": [],
        "body": {
          "mode": "urlencoded",
          "urlencoded": [
            {
              "key": "civilite",
              "value": "Mme",
              "description": "M/Mme",
              "type": "text"
            },
            {
              "key": "nom",
              "value": "NomTest",
              "description": "string",
              "type": "text"
            },
            {
              "key": "prenom",
              "value": "PrenomTest",
              "description": "string",
              "type": "text"
            },
            {
              "key": "addresse",
              "value": "addresse au hazard",
              "description": "string",
              "type": "text"
            },
            {
              "key": "ville",
              "value": "une ville",
              "description": "string",
              "type": "text"
            },
            {
              "key": "code_postale",
              "value": "75015",
              "description": "string",
              "type": "text"
            },
            {
              "key": "courriel",
              "value": "adressemail@gmail.com",
              "description": "string",
              "type": "text"
            },
            {
              "key": "numero_telephone",
              "value": "010203040",
              "description": "string",
              "type": "text"
            },
            {
              "key": "numero_securite_social",
              "value": "564564564",
              "description": "string",
              "type": "text"
            },
            {
              "key": "date_de_naissance",
              "value": "2020-12-04",
              "description": "date YYYY-MM-DD",
              "type": "text"
            },
            {
              "key": "lieu_de_naissance",
              "value": "France",
              "description": "string",
              "type": "text"
            },
            {
              "key": "nationnalite",
              "value": "Francais",
              "description": "string",
              "type": "text"
            },
            {
              "key": "statut",
              "value": "status",
              "description": "string",
              "type": "text"
            },
            {
              "key": "salaire",
              "value": "55464",
              "description": "string",
              "type": "text"
            },
            {
              "key": "poste",
              "value": "sqfsq",
              "description": "string",
              "type": "text"
            },
            {
              "key": "date_d_entree",
              "value": "2020-12-04",
              "description": "date YYYY-MM-DD",
              "type": "text"
            },
            {
              "key": "restaurant",
              "value": "1",
              "description": "foreign key",
              "type": "text"
            }
          ]
        },
        "url": "http://127.0.0.1:8000/staff/personnel/"
      },
      "response": []
    },
    {
      "name": "Create a user",
      "id": "2bb1c227-e962-4ae5-b89b-f4ad6413cb5a",
      "request": {
        "method": "POST",
        "header": [],
        "body": {
          "mode": "urlencoded",
          "urlencoded": [
            {
              "key": "email",
              "value": "newuser@gmail.com",
              "type": "text"
            },
            {
              "key": "password",
              "value": "password123",
              "type": "text"
            }
          ]
        },
        "url": "http://127.0.0.1:8000/accounts/user/"
      },
      "response": []
    },
    {
      "name": "Create a restaurant",
      "id": "fed56926-3429-41dd-b092-66889d7fccca",
      "request": {
        "auth": {
          "type": "bearer",
          "bearer": {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjEwNjMwNzg5LCJqdGkiOiIzMWQxM2NlZDFjNmQ0ZTcwODMzYTUxOWY3NzkzZTAzNyIsInVzZXJfaWQiOjR9.qzG_-HR3khhiFDvygPlmefGlrXGzrZUSOoEy_AIfUAY"
          }
        },
        "method": "POST",
        "header": [],
        "body": {
          "mode": "urlencoded",
          "urlencoded": [
            {
              "key": "name",
              "value": "nom du restau",
              "type": "text"
            },
            {
              "key": "address",
              "value": "l'adresse du restaurant",
              "type": "text"
            },
            {
              "key": "postal_code",
              "value": "98745",
              "type": "text"
            },
            {
              "key": "phone_number",
              "value": "012564654",
              "type": "text"
            },
            {
              "key": "city",
              "value": "nom de la ville",
              "type": "text"
            }
          ]
        },
        "url": "http://127.0.0.1:8000/main/restaurant/"
      },
      "response": []
    },
    {
      "name": "Create a company",
      "id": "d5ecc13e-d35c-47ec-a2b2-e244c98234e8",
      "request": {
        "auth": {
          "type": "bearer",
          "bearer": {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjEwNjMwNzg5LCJqdGkiOiIzMWQxM2NlZDFjNmQ0ZTcwODMzYTUxOWY3NzkzZTAzNyIsInVzZXJfaWQiOjR9.qzG_-HR3khhiFDvygPlmefGlrXGzrZUSOoEy_AIfUAY"
          }
        },
        "method": "POST",
        "header": [],
        "body": {
          "mode": "urlencoded",
          "urlencoded": [
            {
              "key": "name",
              "value": "nom de l'entrerpise",
              "type": "text"
            },
            {
              "key": "address",
              "value": "adresse de l'entreprise",
              "type": "text"
            },
            {
              "key": "postal_code",
              "value": "75012",
              "type": "text"
            },
            {
              "key": "city",
              "value": "Paris",
              "type": "text"
            },
            {
              "key": "capital",
              "value": "45564564",
              "type": "text"
            },
            {
              "key": "SIRET_number",
              "value": "45564564",
              "type": "text"
            },
            {
              "key": "retirement_fund_name",
              "value": "45564564",
              "type": "text"
            },
            {
              "key": "retirement_fund_address",
              "value": "45564564",
              "type": "text"
            },
            {
              "key": "retirement_fund_city",
              "value": "45564564",
              "type": "text"
            },
            {
              "key": "retirement_fund_postal_code",
              "value": "75012",
              "type": "text"
            }
          ]
        },
        "url": "http://127.0.0.1:8000/main/company/"
      },
      "response": []
    },
    {
      "name": "Create a restaurant owner",
      "id": "43f36097-6198-4cc9-8a03-5bd160037388",
      "request": {
        "auth": {
          "type": "bearer",
          "bearer": {
            "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ0b2tlbl90eXBlIjoiYWNjZXNzIiwiZXhwIjoxNjEwNjMwNzg5LCJqdGkiOiIzMWQxM2NlZDFjNmQ0ZTcwODMzYTUxOWY3NzkzZTAzNyIsInVzZXJfaWQiOjR9.qzG_-HR3khhiFDvygPlmefGlrXGzrZUSOoEy_AIfUAY"
          }
        },
        "method": "POST",
        "header": [],
        "body": {
          "mode": "urlencoded",
          "urlencoded": [
            {
              "key": "phone_number",
              "value": "654321023423",
              "type": "text"
            },
            {
              "key": "company_position",
              "value": "sdfqsdfsqdf",
              "type": "text"
            },
            {
              "key": "restaurant",
              "value": "3",
              "type": "text"
            },
            {
              "key": "email",
              "value": "email123@gmail.com",
              "type": "text"
            },
            {
              "key": "user",
              "value": "1",
              "type": "text"
            }
          ]
        },
        "url": "http://127.0.0.1:8000/accounts/restaurantowner/"
      },
      "response": []
    }
  ]
}
```

## A4.2.1 Analyse et correction d'un dysfonctionnement, d'un problème
Lorsque j'ai montré le nouveau back-end à mon tuteur, nous avons rencontré un bug lors de l'authentification. À l'aide de l'outil git, j'ai pu retracé l'origine du bug en retrouvant le commit lors duquel il est apparu, ainsi, je peux le résoudre plus facilement et faire un commit "correction".

{{< picture "markus-github.png" "markus-github.png" "Capture écran du Github de Markus" >}}

## A4.2.2 Adaptation d'une solution applicative aux évolutions
Développement de la version V1 avec les nouveaux liens entre les entités, ils sont adapté désormais aux nouvelles fonctionnalités de l'application Markus.

{{< picture "markus-v1.png" "markus-v1.png" "Capture écran du Github de Markus" >}}

## A5.2.2 Veille technologique
Je me suis documenté sur les APIs d'Uber Eats pour expliqué à mon tuteur lors d'un appel comment nous allions l'intégrer à notre application, je n'ai pas codé cette partie de l'application, car cela est prévue pour une version future.


{{< picture "markus-uber.png" "markus-uber.png" "Capture écran du Github de Markus" >}}

De plus, je me suis renseigné sur les tarifs d'hébergement, comme par exemple Heroku, Linode, etc.. Et nous avons fini par choisir Heroku pour la simplicité de déploiement.
Le déploiement ce fait simplement avec un "git push".


{{< picture "markus-heroku.png" "markus-heroku.png" "Capture écran du Github de Markus" >}}

## A5.2.3 Repérage des compléments de formation ou d'auto-formation
J'ai formé mon équipe de développeurs au framework Django lors d'un appel. Voici une capture d'écran de ce que je leur ai montré :

{{< picture "markus-formation.png" "markus-formation.png" "Capture écran du Github de Markus" >}}
