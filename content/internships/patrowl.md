+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
author = "Raj Porus Hiruthayaraj"
title = "Mon stage chez Patrowl"
tags = [
"internship",
]
thumbnail = "images/patrowl-thumbnail.png"
weigth = 100
+++



## Comment ai-je trouvé ce stage ?

J'ai trouvé ce stage grâce à la plateforme de recrutement Indeed. Après y avoir déposé mon CV, j'ai reçu
un message de la part de mes recruteurs me demandant mes disponibilités pour un entretien à distance. S'en est suivi un entretien en vidéoconférence.
C'est le premier entretien où j'ai eu véritablement à répondre à des questions techniques sur le code.
En sortant de cet appel, je me disais que j'aurais pu mieux répondre à
certaines questions, mais une semaine après, je reçois un mail pour un autre entretien pour
cette fois-ci parler avec un autre développeur.
Durant cet appel, pas de questions techniques, mais seulement administratifs. 
Plus tard, je leur envoie une convention pour confirmer le stage.


## En quoi consiste Patrowl.io ?

Patrowl est un outil déjà existant permettant d'orchestrer de nombreuses attaques de pénétrations. Le code de Patrowl était anciennement utilisé par la Banque de France, cette dernière à accepter de laisser le code open source. Mon tuteur de stage, monsieur Nicolas Mattiocco ayant travaillé pour eux a récupéré le code pour créer une version améliorée de cet outil. Cette entreprise a pour but de transformer cet outil en une plateforme SAAS où l'on pourrais créer un compte et mettre nos "assets", cela peut être une adresse IP, une URL, toutes les choses qu'on souhaite protéger. L'outil fera les tests de manière périodique et remontera aux utilisateurs les "findings", les trouvailles qu'il aura fait en termes de vulnérabilités.

## Ma contribution

L'outil Patrowl dans sa version initiale ne possède pas tout les cloisonnements et les systèmes de permissions qu'on pourrait retrouver dans une plateforme SAAS, de plus il n'y a aucuns tests unitaire. Pour fournir une version payante, les tests unitaires sont impératifs pour assurer une meilleure qualité de services aux utilisateurs, en minimisant les risques de bugs.
De plus, l'ancienne version de Patrowl n'utilise pas correctement les composants du framework Django Rest Framework.

Ma mission consistais à intervenir sur tous ces aspects : j'écris des tests unitaire, je m'assure de
bien utiliser le framework Django Rest Framework et j'écris les tests de permissions ainsi que la logique des cloisonnements.

## A1.2.4 Détermination des tests nécessaires à la validation d'un service

J'ai tout d'abord écris un test pour chaque fonctionnalité, ensuite, il s'agit d'écrire des tests invalides pour ces fonctionnalités. Lorsque j'ai écris le premier test, j'ai déplacé l' "issue" Github dans la section "in progress", et une fois tout les tests écrits, je les ai mis dans la section "tested".
Après avoir fini tout ça, j'ai demandé à mon tuteur une révision de mon code. Voir l'image ci-dessous (participation à un projet).

## A1.4.1 Participation à un projet
Le projet ici consiste à transformer l'outil Patrowl en une plateforme SAAS, pour tout le suivi
de la progression du projet on utilise les issues Github :


{{< picture "patrowl-projet.png" "patrowl-projet.png" "Image alt text" >}}
## A4.1.3 Conception ou adaptation d'une base de données
Voici une fonction que j'ai codée pour importer une "policy" depuis un fichier json indépendamment de l'id de ce dernier car lors de l'importation sur une autre machine l'id ne serait pas le même.
```
def createPolicyFromJSON(data):
newPolicy = EnginePolicy.objects.create(
name=data["name"],
default=data["default"],
description=data["description"],
options=data["options"],
status=data["status"],
)
for scopeName in data["scopes"]:
newPolicy.scopes.add(EnginePolicyScope.objects.get(name=scopeName))
newPolicy.save()
return EnginePolicySerializer(newPolicy).
```
## A4.1.8 Réalisation des tests nécessaires à la validation d'éléments
Après avoir codé mes tests comme cité précédemment je lance cette commande pour m'assurer que tous les tests marchent :

```
python manage.py test -v 3
```
"-v 3" donne une meilleure verbosité, essentiel pour bien débuguer.

## A4.2.1 Analyse et correction d'un dysfonctionnement, d'un problème
L'un des avantages de coder des tests c'est de régler les bugs avant même de pousser le code sur le répertoire, l'une des fausses erreurs que j'ai souvent eu pouvait être fixé par cette ligne. Lorsque que je code un "end-point" de l'api moi-même, Django renvoie par défaut 200 : status ok, mais les tests attendent le statut 201 lors de la création d'une entité, donc j'ai du uniformisé ces statuts à travers les tests et les requêtes.

```
self.assertEqual(response.status_code, 201)
```

## A5.2.4 Étude d'une technologie, d'un composant, d'un outil ou d'une méthode
J'ai appris par moi-même la librairie Python "Celery" qui permet d'accéder à un réseau de RabbitMQ.


{{< picture "patrowl-celery.png" "patrowl-celery.png" "Image alt text" >}}
