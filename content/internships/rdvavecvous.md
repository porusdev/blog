+++
author = "Raj Porus Hiruthayaraj"
title = "Mon premier stage de développeur"
tags = [
"internship",
]
thumbnail = "images/rdvavecvous-thumbnail.png"
weight=100
+++



{{< picture "rdvavecvous-thumbnail.png" "rdvavecvous-thumbnail.png" "Image alt text" >}}

## Comment ai-je trouvé ce stage ?

Tout d'abord, il faut savoir que durant cette période, c'était le début
de la crise du COVID19, il était compliqué de trouver un stage.
J'ai posté mon CV sur un groupe Facebook de petites entreprises,
dès lors, j'ai été contacté par l'entreprise rdvavecvous.

## En quoi consiste rdvavecvous.com ?

Ce site web a pour but de mettre en relation les artisans (plombiers, peintres etc…) avec des clients. Ces derniers peuvent dès lors choisir la plage horaire qui leur convient. L’artisan de son côté peut voir les missions disponibles autour de lui et décider d’intervenir lorsqu’il est disponible.

## Ma contribution

Lors de ce stage, j’ai travaillé avec le langage PHP et le framework Laravel. Mes tuteurs du stage m’ont confié des tâches portant sur le front-end et le style plutôt que sur les fonctionnalités techniques du site.

Mes missions se déroulaient de la manière suivante :
- Ma tutrice effectue des captures écran des modifications qu'elle souhaite voir sur son site
- J'effectue ces modifications et je lui retourne des preuves photo ou vidéo
- Elle les approuve, et je lui envoie le code, dans le cas contraire, je retourne coder les modifications exigées

Exemple de capture écran :

{{< picture "rdvavecvous-screenshot.png" "rdvavecvous-screenshot.png" "Image alt text" >}}

## A1.4.1 Participation à un projet
Au cours de ce stage j'ai participé au projet d'harmonisation entre le front-end vitrine (joomla) et l'application qui est écrite en PHP Laravel, ces deux plateformes possèdent deux URL différents, j'ai :

```
- Aidé ma tutrice à apporter des modifications sur la partie en Joomla
- Apporter les changements de style
- Arranger la responsivité
- Harmoniser les couleurs
```
## A2.3.2 Proposition d'amélioration d'un service
J'ai fait preuve de beaucoup de forces de propositions sur les modifications exigées par ma tutrice de stage.
Notamment celle de créer une animation sur la charte de l’utilisateur qui listerait les points un à un.

- La charte statique
{{< picture "rdvavecvous-charte.png" "rdvavecvous-charte.png" "Image alt text" >}}
- La charte animée
{{< picture "rdvavecvous-charte.gif" "rdvavecvous-charte.gif" "Image alt text" >}}
## A4.1.2 Conception ou adaptation de l'interface utilisateur d'une solution
J'ai adapté l'interface aux exigences de ma tutrice comme montré plus haut.
