+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Le kernel Linux"
author = "Raj Porus Hiruthayaraj"
tags = ["linux"]
+++

Le kernel est un composant qui se charge de la liaison entre les ressources matériels (hardware) de l’ordinateur et la partie logicielle (software). C’est en quelque sorte l’API qui se trouve entre les logiciels et les ressources matériels.

Pour s’assurer que son système d’exploitation tourne sous toutes les machines indifféremment des ressources de cette dernière, on ne peut coder un système qui tournerait par exemple qu’avec une certaine architecture de processeur et une certaine quantité de RAM et des périphériques spécifiques.

Vous l’aurez compris, le kernel est le composant qui va s’occuper de toutes ces contraintes, il englobe par conséquent les pilotes (les programmes nécessaires à la reconnaissance d’un périphérique par un ordinateur).

Je vous souhaite une très bonne lecture.

\- Raj Porus Hiruthayaraj.

{{< picture "linux-kernel-thumbnail.png" "linux-kernel-thumbnail.png" "Image alt text" >}}
