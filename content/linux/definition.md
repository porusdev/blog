+++
title = "Qu'est-ce qu'un kernel ?"
author = "Raj Porus Hiruthayaraj"
tags = ["linux"]
weight = 1
+++

Le Kernel est un composant qui se charge de la liaison entre les ressources matériels (hardware) de l’ordinateur et la partie logicielle (software).
C'est en quelque sorte l'API qui se trouve entre les logiciels et les ressources matériels.

Pour que son système d'exploitation tourne sous toutes les machines indifféremment des ressources de cette dernière, il faut absolument un 
kernel.
En l'absence de ce dernier votre système ne tournera que sur une architecture spécifique, avec une quantité de RAM prédéfini, etc...
Vous l'aurez compris, le kernel est le composant qui va s'occuper de toutes ces contraintes, il englobe par conséquent les pilotes (les programmes nécessaire à la reconnaissance d'un périphérique par un ordinateur).


{{< picture "kernel-schema.png" "kernel-schema.png" "Image alt text" >}}
