+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Les kernels à travers le temps"
author = "Raj Porus Hiruthayaraj"
tags = ["linux"]
weight = 4
+++

Nous allons aborder ici l'historique des kernel tout domaines et types confondus. Ici, on s'intéresse seulement à leur chronologie.

## 1969 - RC 4000 multiprogramming system
On retrouve déjà l'idée d'un programme réutilisable, car avant cela, les programmes tournaient de manière "bare-metal". Par conséquent, un programme était prédisposé à tourner uniquement sur un modèle d'ordinateur bien défini. Le RC multiprogramming system avait pour objectif de s'adapter à n'importe quel système et programme pour exécuter plusieurs programmes à la fois.
Il donnera plus tard l'approche que va prendre le microkernel.
## 1985 - Commodore Amiga
Le kernel du Commodore Amiga s'appelle Exec, et c'est l'un des premiers kernel à être disponible sur une machine grand public. Le type de kernel choisi ici est le modèle de microkernel.

## Les kernels Unix
Unix est une famille de systèmes d'exploitation, ils ont pour point commun une certaine modularité dans leur conception.
Je ne vais pas vous parler de toute la philosophie derrière Unix, mais il s'agit de se focaliser ici sur le point qui est le plus important pour nous. L'un des aspects d'Unix est que "everything is a file" autrement dit, tout est un fichier.
Par exemple, une imprimante ou une caméra frontale peut être traitées comme un fichier.
Mpv est un lecteur de vidéo, mais vous pouvez aussi l'utiliser ainsi :
```
mpv /dev/video0
```
Vous pouvez voir le rendu de la webcam ainsi, "/dev/video0" est l'emplacement dans lequel se trouve le fichier correspondant à la caméra.

Voici une frise chronologique de ces kernels dérivés d'Unix :

{{< picture "unix-kernel-evo.png" "unix-kernel-evo.png" "Image alt text" >}}

## Sources 
- [Wikipedia sur les kernels](https://en.wikipedia.org/wiki/Kernel_(operating_system))
- [PDF RC 4000 SOFTWARE:MULTIPROGRAMMING SYSTEM](http://cseweb.ucsd.edu/classes/wi20/cse291-i/hansen69.pdf)
- [Le wiki d'Amiga, rubrique du kernel](https://wiki.amigaos.net/wiki/Programming_AmigaOS_4:_Exec_-_The_Kernel)
