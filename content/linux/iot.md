+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Linux dans l'IoT"
author = "Raj Porus Hiruthayaraj"
tags = ["index"]
weight = 6
+++

Le kernel Linux est très répandue dans l'IoT et grâce à sa modularité elle permet de s'adapter à n'importe quelle machine.
Il existe quelque distribution spécialement faite pour l'IoT, nous allons passer certaines de ces distributions en revue.

## Adapter le kernel pour l'IoT

Des modifications sont effectuées pour que le kernel de Linux soit le plus léger pour les appareils connectés.
Lors de compilation du kernel on peut passer l'arguement [-Os](https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html) à gcc
pour optimiser la compilation par rapport à l'espace mémoire, mais en revanche, ce processus prend plus de temps.
Une autre commande pour réduire la taille du kernel est d'utiliser "make tinyconfig" au lieu de "make menuconfig".
Avec tinyconfig, le kernel va prendre automatiquement les options pour qu'il soit le plus petit possible.
On avait vu plus tôt l'utilisation de BusyBox par Alpine Linux pour alléger le système, il existe un autre ensemble de
coreutils plus minimal qui est "toybox".

## Yocto
Le projet yocto vous laisse faire votre propre distribution dédiée à l'IoT. Vous pouvez donc choisir les fonctions que
vous voulez et retirer tout le reste, le rendant alors plus légers. La légerté est très recherché pour les objets de l'Iot
qui sont petit et n'ont pas une configuration très performante.

## Android Things
Comme à l'image d'Android, Android Things utilise aussi le kernel de Linux. Avec les APIs et les ressources d'Android,
cette distribution est à préférer pour faire un objet connecté avec un téléphone sous Android.
Les hardwares supportés d'après leur site sont :
- Le NXP Pico i.MX7D
- Le Raspberry Pi 3 Model B

## Ubuntu Core

Ubuntu est connu dans la communauté de GNU/Linux comme l'une des distributions les plus surchargées.
Mais Ubuntu Core en revanche est plus minimal et pensé pour des appareils embarqués.
Ce système se veut très sécurisé et comme ayant un répertoire de paquets stable pour les objets connectés.

## Raspbian

Raspbian se base sur Debian, et c'est une distribution spécialement faite pour les Raspberry Pi.
Debian étant très stable, cette distribution peut être privilégiée pour les Raspberry Pi. De plus, il offre la garantie de
marcher sur les Raspberry Pi tandis que d'autres distributions peuvent parfois demander des changements manuels.

## Debian Tinker

Debian Tinker est un sous-projet de Debian, et c'est sans doute la distribution de cette liste qui peut marcher sur le plus
d'hardware, certaines personnes ont réussi à le faire marcher sur les smartphones.
Ce système possède durant l'installation de nombreux pilotes pour les appareils les moins utilisés.

## PX4 Drone Autopilot
Pour étudier un projet dans ce domaine de L'IoT on va choisir les drones et en particulier le projet qui porte le nom de PX4 Autopilot.
Ce projet est soutenu par Dronecode, qui est une fondation qui apporte un support aux projets d'innovations open-source dans le secteur du drone, cette fondation est elle-même rattaché à la Linux Foundation, la boucle est bouclé.
PX4 Drone Autopilot est un autopilote open source qui est compatible avec les drone sous :
- GNU/Linux
- NuttX
- QuRT

PX4 peut aussi tourner sous Docker, technologie qui supporte GNU/Linux comme on a vu précédemment.
Un composant sur les drones appelé [USB FTDI](https://store.mrobotics.io/USB-FTDI-Serial-to-JST-GH-p/mro-ftdi-jstgh01-mr.htm) sert à contrôler des accessoires comme les GPS par exemple, et sous Linux l'adresse dans le système de ce composant
est dans /dev/ttyUSB0.

## Schéma du fonctionnement du drone
{{< picture "stack-px4.svg" "stack-px4.svg" "Image alt text" >}}
{{< picture "flight-stack.png" "flight-stack.png" "Image alt text" >}}
{{< picture "pixhawk-raspberry.jpg" "pixhawk-raspberry.jpg" "Image alt text" >}}
## Répartition des tâches
{{< picture "px4-kernel.png" "px4-kernel.png" "Image alt text" >}}
## Les projets est entreprises autour de PX4
{{< picture "px4-projects.png" "px4-projects.png" "Image alt text" >}}
## Avantages et inconvénients à utiliser les pilotes du kernel Linux
Les avantages sont les suivants :
- Plusieurs pilotes déjà disponibles
- Possibilité de tester sur plusieurs plateformes (Linux desktop, Android)
- Réduire la complexité du "stack" de vol
- Réduire la complexité de la communication avec les capteurs

Les inconvénients sont les suivants :

- Certaines plateformes populaire parmi les drones sont incompatibles avec ces pilotes (PX4 Middleware / Nuttx)
- Plus compliqué de faire des prototypes de drivers
- Les capteurs actuels n'ont pas encore de pilotes dans ce kernel

En somme, PX4 Drone Autopilot a pour ambition de devenir un incontournable parmis les systèmes de vol autonome.
Pour cela, il veut mettre en place les standards de cette industrie et assurer une grande compatibilité pour les entreprises voulant 
utiliser leur système. La compatibilité est assuré par le support du kernel Linux. Linux étant totalement indépendant,
on ne risque pas de rencontré des soucis de dépendances à une entreprise en particulier.

## Sources 

- [Les distros de Linux utilisé dans l'IoT](https://www.iottechtrends.com/linux-distros-in-iot-devices/)
- [7 projets d'IoT qui utilisent Linux](https://www.networkworld.com/article/3200272/the-top-7-linux-iot-projects.html)
- [Github PX4 Drone Autopilot](https://github.com/PX4/PX4-Autopilot)
- [Le site de PX4 Drone Autopilot](https://px4.io/)
- [Shrinking the Linux Kernel and File System for IoT](https://www.linux.com/news/shrinking-linux-kernel-and-file-system-iot/)
- [Past, present, future of PX4 - Dr. Lorenz Meier - PX4 Developer Summit 2019](https://youtu.be/Lzw7sWLCfLc)
- [Linux - The future for drones](https://elinux.org/images/c/c5/Linux_-_The_Future_For_Drones.pdf)
