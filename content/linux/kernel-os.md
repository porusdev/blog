+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Linux n'est pas un système d'exploitation"
author = "Raj Porus Hiruthayaraj"
tags = ["index"]
weight = 2
+++

L’une des erreurs les plus récurrentes et de confondre kernel et système d’exploitation dans le cas de "Linux".
Cet abus de langage est tellement répandu que je juge utile de faire une parenthèse là-dessus.
Les personnes qui utilisent le terme “Linux” pour parler de système d’exploitation comme Debian par exemple utilisent mal ce terme. Ce qu’ils devraient dire est “GNU/Linux”, GNU désignant la partie Logicielle, et Linux étant le kernel, ils marchent ensemble, mais leurs fonctions sont très différentes.

## Copypasta

Cette erreur est tellement commune au sein de la communauté des utilisateurs de GNU/Linux qu’un texte copié-collé circule sur les forums de cette communauté afin de rectifier ces utilisateurs de manière un peu comique sur le terme à utiliser.

```
“I’d just like to interject for a moment.
What you’re referring to as Linux, is in fact, GNU/Linux,
or as I’ve recently taken to calling it, GNU plus Linux.
Linux is not an operating system unto itself, but rather
another free component of a fully functioning GNU system made
useful by the GNU corelibs, shell utilities and vital system components
comprising a full OS as defined by POSIX.
Many computer users run a modified version of 
the GNU system every day, without realizing it.
Through a peculiar turn of events, the version of 
GNU which is widely used today is often called “Linux”, 
and many of its users are not aware that it 
is basically the GNU system, developed by the GNU Project.
There really is a Linux, and these people are using it, 
but it is just a part of the system they use.
Linux is the kernel: the program in the system that allocates 
the machine’s resources to the other programs that you run.
The kernel is an essential part of an operating system, but useless 
by itself; it can only function in the context of a complete operating system.
Linux is normally used in combination with the GNU operating system: the 
whole system is basically GNU with Linux added, or GNU/Linux.
All the so-called “Linux” distributions are really distributions of GNU/Linux.”
```

## GNU core utilities

Les "GNU core utilities" sont le regroupement d'un ensemble de commande de base qu'utilisent les utilisateurs du
système GNU/Linux.
https://www.gnu.org/software/coreutils/coreutils.html

En effet, ce n'est qu'un abus de langage, car toutes ces commandes de base utilisée dans leur système sont originaires du projet GNU en majorité. Vous ne me croyez pas ?

Ouvrez donc votre terminal sous votre système GNU/Linux, saisissez la commande suivante :

```
man grep
```

Pour ceux qui ne sont pas connaisseur des commandes du terminal, il s'agit là de deux commandes, "grep" est une espèce de "ctrl+f" surpuissant, il permet de trouver des paternes de textes dans des fichiers, et même des répertoires. La commande qui précède celle-ci est "man", elle fournit une explication sur la commande qui suit donc ici "grep".

Appuyez maintenant sur "G" ou "shift + g" vous seriez alors en bas de page, vous verrez qu'il y aura écrit "GNU grep".

Bien sûr, cela ne s'applique pas par exemple si vous utilisez un autre système tel qu'OpenBSD par exemple, vous aurez alors leur version de grep.

L'un des seuls systèmes ayant le kernel Linux mais sans les commandes de GNU est par exemple Alpine Linux, qui utilise d'autre coreutils, dans ce cas là il s'agit de busy box coreutils. https://www.busybox.net/

Pour conclure, ces commandes, faisant partie de la couche logicielle, appartiennent à GNU et Linux est le kernel qui fournit les ressources de l'ordinateur seulement.

## Android

Android utilise Linux, si on suit mon raisonnement vous aurez compris qu'on parle là du kernel, et en effet Android a seulement Linux comme kernel, les similarités entre Android et votre système Ubuntu par exemple, s'arrêtent là.
Il ne faut surtout pas croire qu'Android est un système GNU/Linux, il n'est pas libre et open source et il utilise des coreutils propriétaires.
