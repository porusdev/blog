+++
author = "Raj Porus Hiruthayaraj"
title = "Linux"
date = "2020-12-04"
tags = [
    "linux",
]
weight = 5
thumbnail = "images/linux-kernel-thumbnail.png"
+++

Ici, nous allons parler d’un kernel en particulier et ses spécificités pour mieux comprendre le fonctionnement des kernels. J’ai choisi pour cela le kernel Linux, car c’est sans doute le plus répandu et le mieux documenté.

## GNU/Linux une très belle rencontre

En 1983, un certain Richard Stallman travaillait sur un système qui s’appelle GNU, il est dérivé d’Unix d’ailleurs GNU signifie, GNU is Not Unix. Pourquoi marquer cette distinction ? Car Unix est un logiciel propriétaire, et Richard Stallman et l’un des premiers activiste du mouvement du logiciel libre. Il sera plus tard à l’origine du FSF (Free Software Foundation) et va créer les licences GNU GPL que l’on connaît aujourd’hui. En 1992, ce système GNU n’était toujours pas abouti, car il lui manquait un kernel. Un kernel nommé GNU Hurd était en développement, mais il n’était pas abouti et toujours très loin de la maturation que peut avoir Linux aujourd’hui. Aujourd’hui GNU Hurd et ses sorties de nouvelles versions sont moqués par la communauté d’utilisateurs de GNU/Linux pour l’immense retard qu’il a en termes de fonctionnalités. Déjà en 1991, Linus Torvalds travaillait sur un kernel de manière indépendante. C’est leur rencontre en 1992 qui va donner GNU/Linux.

### Concept de "Freedom"

GNU vient avec des règles établi par Richard Stallman et sa vision du logiciel libre, sa vision se caractérise par 4 principes :

- freedom zero : N’importe qui peut utiliser le logiciel, pour n’importe quel usage.
- freedom one : Tout le monde est libre d’étudier le fonctionnement du logiciel et d’y apporter le changement qu’il souhaite.
- freedom two : Le logiciel est redistribuable à quiconque.
- freedom three : Chaque personne a la liberté de publier la version du logiciel qu’il a modifié afin que les autres puissent en profiter.

Depuis l’alliance de GNU et de Linux, Linux se voit aussi soumis à ces principes.
## Conception

Le kernel Linux est monolithique et modulaire, car on peut insérer et retirer des modules dans le kernel. Nous verrons plus tard en détail 
à quoi correspondent ces modules. 
Ce diagramme illustre la répartition des tâches entre l'espace kernel et l'espace utilisateur :

{{< picture "linux-tasks-kernel.png" "linux-tasks-kernel.png" "Image alt text" >}}

Une citation de Linus Torvalds décris le mieux à mon sens, la conception derrière Linux :

"Linux is evolution, not intelligent design!"

\- Linus Torvalds, 2005

Cette citation prends tout son sens quand on sait que le kernel est open source et qu'une grande communauté contribue massivement à son code.
Linux est soumis à la licence GPLv2 et son code est hébergé sur un répertoire Git. Les changements sont apportés via LKML (Linux Kernel Mailing List) sous la forme de patchs. Une certaine méthodologie est exigée de la part des développeurs qui souhaitent contribuer au kernel.
Aujourd'hui, Linux ne compte pas moins de 30.34 millions de lignes de codes.

## Modules 

Les modules sont essentiels aux fonctionnements du kernel de Linux, c’est ça qui lui permet d’avoir une grande possibilité de personnalisation. Une distribution de GNU/Linux appelé Gentoo a une philosophie de tout compiler soi-même sur sa machine, et même le kernel. Les utilisateurs de Gentoo sont ceux qui compilent le plus souvent leur propre kernel, mais cette fonction n’est pas unique à leur distribution.

{{< picture "gentoo-radeon-kernel-config.png" "gentoo-radeon-kernel-config.png" "kernel compiler in ncurse">}}

Dans le menu ci-dessus en Ncurse, il est possible de cocher et décocher les modules que l'on veut sur son kernel.

Voici une petite liste des options que vous pouvez activer à travers ce menu, vous pouvez retrouver la liste complète [ici](https://raw.githubusercontent.com/MentalOutlaw/deploygentoo/master/gentoo/kernel/gentoohardenedminimal):
```
#
# Automatically generated file; DO NOT EDIT.
# Linux/x86 5.8.6-gentoo Kernel Configuration
#
CONFIG_CC_VERSION_TEXT="gcc (Gentoo Hardened 10.2.0-r1 p2) 10.2.0"
CONFIG_CC_IS_GCC=y
CONFIG_GCC_VERSION=100200
CONFIG_LD_VERSION=234000000
CONFIG_CLANG_VERSION=0
CONFIG_CC_CAN_LINK=y
CONFIG_CC_CAN_LINK_STATIC=y
CONFIG_CC_HAS_ASM_GOTO=y
CONFIG_CC_HAS_ASM_INLINE=y
CONFIG_IRQ_WORK=y
CONFIG_BUILDTIME_TABLE_SORT=y
CONFIG_THREAD_INFO_IN_TASK=y

#
# General setup
#
CONFIG_INIT_ENV_ARG_LIMIT=32
# CONFIG_COMPILE_TEST is not set
CONFIG_LOCALVERSION=""
# CONFIG_LOCALVERSION_AUTO is not set
CONFIG_BUILD_SALT=""
CONFIG_HAVE_KERNEL_GZIP=y
CONFIG_HAVE_KERNEL_BZIP2=y
CONFIG_HAVE_KERNEL_LZMA=y
CONFIG_HAVE_KERNEL_XZ=y
CONFIG_HAVE_KERNEL_LZO=y
CONFIG_HAVE_KERNEL_LZ4=y
# CONFIG_KERNEL_GZIP is not set
# CONFIG_KERNEL_BZIP2 is not set
# CONFIG_KERNEL_LZMA is not set
# CONFIG_KERNEL_XZ is not set
# CONFIG_KERNEL_LZO is not set
CONFIG_KERNEL_LZ4=y
CONFIG_DEFAULT_INIT="6"
CONFIG_DEFAULT_HOSTNAME="(none)"
CONFIG_SWAP=y
CONFIG_SYSVIPC=y
CONFIG_SYSVIPC_SYSCTL=y
# CONFIG_POSIX_MQUEUE is not set
# CONFIG_WATCH_QUEUE is not set
# CONFIG_CROSS_MEMORY_ATTACH is not set
# CONFIG_USELIB is not set
# CONFIG_AUDIT is not set
CONFIG_HAVE_ARCH_AUDITSYSCALL=y

```

Il est possible de personnaliser le kernel à un tel point qu'il soit très minimal et sur-mesure pour votre PC, des utilisateurs de Gentoo
parviennent à avoir une machine qui ne consomme que 25mb au repos.

Des modules externes peuvent être ajoutés, par exemple Nvidia, c'est-à-dire des pilotes qui peuvent parfois être propriétaires ne sont pas dans le kernel par défaut, à l'inverse d'AMD qui est open source. Il faut donc les installer manuellement.
Le manque de compatibilité sous Linux n'est pas tellement problématique, car on peut tout simplement coder un module ou un pilote pour avoir le support d'un périphérique anciennement pas supporté.

Je prends en exemple [xow](https://github.com/medusalix/xow) qui est un pilote tournant dans l'espace utilisateur et permettant d'avoir un
support pour l'adapteur USB sans fil des manettes Xbox One. Pour cela, ils ont étudié les signaux de l'adapteur USB avec wireshark et ils
ont coder un pilote qui fait appel à l'API du kernel pour émuler les pressions de la manette.

D'autres modules peuvent être créés pour des composants virtuels. Pour illustrer cela, j'ai choisi [v4l2loopback](https://github.com/umlaeute/v4l2loopback), ce module permet de créer des fausses webcams.
Avec l'aide d'OBS, un programme pour la capture d'écran et de streaming, on pouvait donner le flux de notre bureau à cette webcam virtuelle.
Ainsi, les utilisateurs pouvaient activer leur webcam virtuelle sur Discord par exemple, et au lieu de voir un flux de webcam, on verrait une
partage d'écran du bureau. Ce module a été utilisé sur Discord, car la compression pour un partage d'écran est plus élevée que pour la webcam.

Écrire un module n'est pas aussi compliqué qu'il paraît, nous allons voir dans la prochaine section comment coder un module qui va afficher "hello world".

## Développement 

Nous allons désormais écrire notre propre module de kernel, il n'est connecté à aucun périphérique matériel, c'est alors un module logique, ou virtuelle.

Nous avons besoin de deux fichiers pour cela, le premier, "testmodule.c" qui contient le code en C de notre module, et nous avons ensuite "Makefile" qui énonce les règles de compilation de notre module.

- testmodule.c :
```
#include <linux/init.h>
#include <linux/module.h>
#include <linux/kernel.h>
#include <linux/fs.h>
#include <linux/uaccess.h>

#define DEVICE_NAME "testmodule"
MODULE_LICENSE("GPL v2");
static int dev_open(struct inode*, struct file*);
static int dev_release(struct inode*, struct file*);
static ssize_t dev_read(struct file*, char*, size_t, loff_t*);
static ssize_t dev_write(struct file*, const char*, size_t, loff_t*);

static struct file_operations fops = {
   .open = dev_open,
   .read = dev_read,
   .write = dev_write,
   .release = dev_release,
};

static int major;

static int __init testmodule_init(void) {
    major = register_chrdev(0, DEVICE_NAME, &fops);

    if (major < 0) {
        printk(KERN_ALERT "Testmodule load failed\n");
        return major;
    }

    printk(KERN_INFO "Testmodule module has been loaded: %d\n", major);
    return 0;
}

static void __exit testmodule_exit(void) {
    unregister_chrdev(major, DEVICE_NAME);
    printk(KERN_INFO "Testmodule has been unloaded\n");
}

static int dev_open(struct inode *inodep, struct file *filep) {
   printk(KERN_INFO "Testmodule device opened\n");
   return 0;
}

static ssize_t dev_write(struct file *filep, const char *buffer,
                         size_t len, loff_t *offset) {

   printk(KERN_INFO "Sorry, testmodule is read only\n");
   return -EFAULT;
}

static int dev_release(struct inode *inodep, struct file *filep) {
   printk(KERN_INFO "Testmodule device closed\n");
   return 0;
}

static ssize_t dev_read(struct file *filep, char *buffer, size_t len, loff_t *offset) {
    int errors = 0;
    char *message = "Hello world";
    int message_len = strlen(message);

    errors = copy_to_user(buffer, message, message_len);

    return errors == 0 ? message_len : -EFAULT;
}

module_init(testmodule_init);
module_exit(testmodule_exit);
```
- Makefile :
```
obj-m += testmodule.o

all:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) modules

clean:
	make -C /lib/modules/$(shell uname -r)/build M=$(PWD) clean

```

Maintenant nous avons plus qu'à compiler le tout, saisissez la commande :

```
make
```

Faites un "ls -la" pour voir le contenu du répertoire :
```
[porus@arch Desktop]$ ls -la
total 448
drwx------  2 porus wheel   4096 May  2 09:51 .
drwx------ 18 porus wheel   4096 May  2 09:28 ..
-rw-r--r--  1 porus wheel    160 May  2 09:49 Makefile
-rw-r--r--  1 porus wheel     34 May  2 09:51 modules.order
-rw-r--r--  1 porus wheel    146 May  2 09:51 .modules.order.cmd
-rw-r--r--  1 porus wheel      0 May  2 09:51 Module.symvers
-rw-r--r--  1 porus wheel    188 May  2 09:51 .Module.symvers.cmd
-rw-r--r--  1 porus wheel   1829 May  2 09:44 rickroll.c
-rw-r--r--  1 porus wheel   1827 May  2 09:51 testmodule.c
-rw-r--r--  1 porus wheel 192144 May  2 09:51 testmodule.ko
-rw-r--r--  1 porus wheel    214 May  2 09:51 .testmodule.ko.cmd
-rw-r--r--  1 porus wheel     34 May  2 09:51 testmodule.mod
-rw-r--r--  1 porus wheel    587 May  2 09:51 testmodule.mod.c
-rw-r--r--  1 porus wheel    130 May  2 09:51 .testmodule.mod.cmd
-rw-r--r--  1 porus wheel  33248 May  2 09:51 testmodule.mod.o
-rw-r--r--  1 porus wheel  30457 May  2 09:51 .testmodule.mod.o.cmd
-rw-r--r--  1 porus wheel 104960 May  2 09:51 testmodule.o
-rw-r--r--  1 porus wheel  38165 May  2 09:51 .testmodule.o.cmd

```

Le fichier qui va nous intéresser ici est : "testmodule.ko"
Le ".ko" signifie "kernel object ", c'est lui le module que l'on va injecter dans notre kernel.
Voici les trois commandes les plus importantes à connaître pour injecter un module dans le kernel :
- lsmod, à l'image d'un "ls" qui liste les fichiers dans un répertoire, lsmod liste tous les modules présents dans votre kernel.
- insmod, cette commande va injecter un module dans votre kernel.
- rmmod, comme la commande "rm" permet de supprimer des fichiers, rmmod permet de supprimer un module injecté dans votre kernel.

Il nous faut une autre commande qui va nous permettre de suivre les étapes de l'injection de notre module dans le kernel.
Si vous avez le malheur d'être sous un système de GNU/Linux avec systemd vous devez utiliser la commande suivante pour lire la journalisation de votre kernel :
```
journactl
```
Mais dans le cas contraire où vous n'utilisez pas systemd faites tout simplement : 
```
tail -f /var/log/kern.log
```
Maintenant, laissez une de ces commandes tourner dans un terminal à côté, on y reviendra plus tard.
Pour injecter maintenant notre module ouvrez un autre terminal et saisissez la commande suivante (doas est une commande similaire à sudo, utilisez sudo si vous l'avez sur votre système):
```
doas insmod testmodule.ko
```
En jetant un œil à votre terminal qui affiche la journalisation, vous verrez quelques choses similaire à :
```
May 02 10:16:53 arch doas[22088]: porus ran command insmod testmodule.ko as root from /home/porus/Desktop
...
May 02 10:25:56 arch kernel: Testmodule module has been loaded: 235
```
D'après cette journalisation notre module à bien été pris en compte par le système, cherchez un peu plus loin pour trouver le numéro associé à ce module, dans notre cas, il s'agit de 235. Pour être sûr que le kernel est bien injecté dans un terminal saisissez :
```
lsmod | grep testmodule
```
Le résultat (si tout vas bien) :
```
testmodule             16384  0
```
On va maintenant créer un fichier qui est associé à notre module, on va se rendre au répertoire /dev qui contient tout les appareils sous forme de fichiers, comme le veux la philosophie Unix.
```
cd /dev
```
Nous allons utiliser une commande nommé mknod avec le nom du ficher et le numéro associé à ce dernier pour créer ce fichier :
```
doas mknod testmodule c 235 0
```
- testmodule : le nom arbitraire que nous avons donné au fichier
- 235 : le numéro dit majeure qu'on a trouvé plus haut
- 0 : le numéro mineure
La commande :
```
cat /dev/testmodule
```
Va afficher à l'infini des "hello world", c'est ce que fait notre module mais en parallèle dans votre journalisation vous pouvez voir :
```
May 02 10:30:35 arch kernel: Testmodule device opened
```
Quand cat est executé.
```
May 02 10:30:36 arch kernel: Testmodule device closed
```
Quand cat est arrêté.

On peut maintenant retirer le module avec rmmod :
```
doas rmmod testmodule
```
La journalisation :
```
May 02 10:37:33 arch doas[22569]: porus ran command rmmod testmodule as root from /dev
```
Et il ne s'affiche plus dans lsmod :
```
[porus@arch ~]$ lsmod | grep testmodule
[porus@arch ~]$
```
Par contre notre fichier existe toujours dans /dev/ :
```
[porus@arch ~]$ ls /dev/testmodule
/dev/testmodule
```
Mais en revanche le module ne marchera plus :
```
[porus@arch ~]$ cat /dev/testmodule
cat: /dev/testmodule: No such device or address
```
Pour nettoyer tout ça, on tout simplement suprimmer /dev/testmodule maintenant qu'il ne peut plus marcher :
```
doas rm /dev/testmodule
```
Félicitations vous savez maintenant comment écrire un module pour le kernel Linux.

## Domaines d'applications 

- Serveurs 

Le kernel Linux est retrouvé dans un grand nombre de domaines, car là où vous avez GNU/Linux ce kernel y est forcément.
Les serveurs par exemples utilisent en grande partie GNU/Linux et très souvent la distribution Debian, appréciée pour sa grande stabilité.
Avoir la possibilité de personnaliser son kernel dans un serveur est très avantageux. Il permet de retirer des fonctions jamais utilisées pour
rendre le service plus sécurisé, car la surface de menace se verrait réduite, par exemple la distribution Ubuntu Server ne propose pas de support Wi-fi par défaut
car un serveur est très souvent branché en Ethernet. Avec une personnalisation du kernel il est possible de par exemple totalement retirer le support pour la WiFi
allégant alors le kernel et procurant un petit boost de vitesse au serveur.

- Machine virtuelle et Docker

On retrouve en abondance GNU/Linux dans les machines virtuelles, et récemment, dans des conteneurs Docker.
Docker est une plateforme de développement et une technologie de virtualisation, on code notre programme directement dans cette machine virtuelle,
ainsi, le programme contenu dans Docker marchera de la même manière que ce soit sous Windows, MacOS, ou GNU/Linux.
Les dépendances liées aux programmes sont contenues dans la machine virtuelle et le développeur n'aura pas besoins de se préoccuper de leur installation.
Docker est très utile pour les applications qui tournent sur un serveur car on peut tout simplement déployer l'image Docker sur le serveur et il marchera
comme sur le poste du développeur, sans qu'on installe nous-même les dépendances.
L'une des images de Docker les plus légère est Alpine Linux, c'est une distribution de GNU/Linux qui n'utilise pas les coreutils de GNU, ils utilisent les coreutils de BusyBox qui sont plus légères.
Pour avoir une image plus légère, on peut bien entendu personnaliser le kernel comme le fait Amazon. Amazon possède une distribution réservé aux serveurs appelé Amazon Linux et ils ont
un kernel personnalisé pour leur usage, pour rajouter d'autres fonctions qu'Amazon a décidé de retirer de cette image, on doit recompiler le kernel, cet [article](https://www.artembutusov.com/how-to-rebuild-amazon-linux-kernel-in-amazon-linux/) explique en détail comment le faire sous Amazon Linux.
Enfin, avec des coreutils léger et un kernel personnalisé on peut produire une image très légère de Docker et très performante.

- PC de bureau

Beaucoup de personnes utilisent GNU/Linux sur leurs PC de bureau (y compris moi), ces utilisateurs peuvent chercher pour certains, plus de protection pour la vie privé, une meilleure liberté logicielle
ou bien tout simplement chercher à avoir le système le plus performant possible.
Le kernel monolithique est assez pratique pour les PC de famille, et même sans personnalisation, car par défaut le kernel Linux possède un bon nombre de pilotes pour périphérique dans le kernel.
Il y a rarement besoin d'installer un pilote pour que vos périphériques soient détectés, prenons l'exemple des tablettes graphiques de chez [wacom](https://www.wacom.com/en-us), leur pilote est
déjà dans le kernel, faites "lsmod | grep wacom" et vous le verrez, ce grand nombre de pilotes explique pourquoi le kernel Linux est aussi lourd et c'est pour cela que les utilisateurs qui
recherchent la performance avant tout auront plutôt tendance à le personnaliser et retirer par exemple le module wacom s'ils ne possèdent pas de tablettes graphiques.

On parlera plus un détail d'un domaine d'application et pour cela, j'ai choisi de vous parler de l'Internet des choses (IoT).

## Le futur du kernel de Linux

La Linux Foundation qui maintient le kernel fait face à de nombreuses épreuves quant à l'évolution de ce dernier.
Ces épreuves peuvent être sociales comme techniques et nous allons aborder les obstacles majeurs rencontrés.

### Manque de nouveau 'kernel maintainers'

Linus Torvalds a fait part de ce problème lors de la conférence Open Source Summit et L'Embedded Linux
conference. Une des raisons qu'évoque Linus Torvalds et que le kernel est à la fois intéressant et ennuyant. Les développeurs d'aujourd'hui trouvent plus attrayant de travailler sur des technologies de deep learning ou bien le blockchain, en outre les technologies qui font
la une des journaux. L'autre raison est que le kernel est écrit en langage C, et qu'il est
rare de trouver un bon développeur de C aujourd'hui, car les développeurs modernes ont plus
tendance à utiliser Rust ou Go. Nous allons revenir à la relation qu'à Rust et Linux.

### Rust

L'ajout de Rust est envisagé par Linus Torvalds. Rust a fait ces preuves dans le secteur des OS et kernel comme on a pu le voir dans l'OS Redox.
Cet ajout pourra répondre à la difficulté de trouver des développeurs de C.

### Université de Minnesota

L'université de Minnesota à envoyé en avril 2021 des commits au répertoire de Linux contenant des failles de sécurité.
Cela avait pour but d'après l'université d'étudier à quel point il était facile de compromettre la sécurité d'un projet
open source. L'université n'avait pas prévenu les mainteneurs du kernel avant d'envoyer ces commits, et c'est pour cela
que l'université de Minnesota a été bannie et ne peut plus contribuer au projet de Linux.
Pour leur défense, l'université de Minnesota a affirmé que l'étude n'aurait pas été authentique si les mainteneurs étaient
déjà au courant de la nature des commits et leur menace de sécurité.

Ce qu'avance l'université de Minnesota est très plausible étant donné qu'en mars 2021 le code source de PHP avait était compris par des commits malicieux après avoir piraté leur serveur de Git interne, on peut penser que cette attaque a
donné l'idée à cette Université de conduire une telle étude, mais cette fois-ci sans pirater de serveur git.
Cette attaque de l'université aurait pu être grave, car les mainteneurs de kernel ont parfois une grande confiance dans
les universités qui contribuent à ce projet.

### Blacklist & Whitelist

Suite au mouvement de Black Lives Matter après la mort de George Floyd, il s'en est suivi d'un changement dans le domaine
de l'informatique. Des changements de syntaxes seulement, mais des changements néanmoins.
Par exemple Github renomme les branche 'masters' en branche 'main'.
Les dons fait à la fondation de Linux était déjà versé à des oeuvres de diversité dans le domaine de la technologie,
mais à la suite de ce mouvement, un changement a été apporté au kernel de Linux.
On appelle blacklister dans le kernel de Linux le fait d'empêcher au kernel l'accès à une pièce de l'ordinateur, la
carte graphique par exemple. Blacklist et Whitelist ont été changé en Denylist et Allowlist.

### ARM

Suite à la décision d'Apple d'utiliser des processeurs ARM en juin 2020, Linus Torvalds a affirmé vouloir qu'un jour
on puisse utiliser Linux au quotidien sur des processeurs ARM et développeur sur des processeurs ARM et compiler un
kernel sur les processeurs ARM.

## Sources
- [How Do Linux Kernel Drivers Work? - Learning Resource](https://youtu.be/juGNPLdjLH4)
- [What is GNU? (WSIS, Tunis)](https://youtu.be/p2kcm81t9d8)
- [The Linux Kernel Documentation](https://kernel.readthedocs.io/en/latest/)
- [Linux Device Drivers, Third Edition](https://lwn.net/Kernel/LDD3/)
- [&rsquo;It&rsquo;s really hard to find maintainers&#x2026;&rsquo; Linus Torvalds ponders the future of Linux](https://www.theregister.com/2020/06/30/hard_to_find_linux_maintainers_says_torvalds/)
- [Le répertoire Github du Kernel de Linux](https://github.com/torvalds/linux)
- <https://itsfoss.com/medical-linux-ai-blockchain/>
- [Wikipedia, GNU Project](https://en.wikipedia.org/wiki/GNU_Project)
- [What is free software ?](https://www.gnu.org/philosophy/free-sw.en.html)
- [Wikipedia, Linux](https://en.wikipedia.org/wiki/Linux)
- [How to build a Linux loadable kernel module](https://youtu.be/CWihl19mJig)
- [What is docker ?](https://www.cloudsavvyit.com/490/what-does-docker-do-and-when-should-you-use-it/)
- [L'opinnion de Linus à propos de Rust dans le kernel](https://lkml.org/lkml/2021/4/14/1099)
- [Excuse de l'université de Minnesota à Linux](https://linux.slashdot.org/story/21/04/25/0243259/university-of-minnesota-researchers-send-apology-to-linux-kernel-mailing-list)
- [Hackers backdoor PHP source code after breaching internal git server](https://arstechnica.com/gadgets/2021/03/hackers-backdoor-php-source-code-after-breaching-internal-git-server/)
