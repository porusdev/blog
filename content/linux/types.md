+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Différents types de kernels"
author = "Raj Porus Hiruthayaraj"
tags = ["index"]
weight = 3
+++

Il existe plusieurs types de kernels, mais pour faire simple voici un tableau comparant les deux modèle de kernel les plus répandues :

<table border="2" cellspacing="0" cellpadding="6" rules="groups" frame="hsides">


<colgroup>
<col  class="org-left" />

<col  class="org-left" />

<col  class="org-left" />
</colgroup>
<thead>
<tr>
<th scope="col" class="org-left">Type de kernel</th>
<th scope="col" class="org-left">Avantage</th>
<th scope="col" class="org-left">Incovégiant</th>
</tr>
</thead>

<tbody>
<tr>
<td class="org-left">Monolithique</td>
<td class="org-left">- rapide car il est un seul composant</td>
<td class="org-left">- la taille du kernel est plus grande</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">- moins de code</td>
<td class="org-left">- peu fiable, car un service peut faire échouer tout le système</td>
</tr>
</tbody>

<tbody>
<tr>
<td class="org-left">Microkernel</td>
<td class="org-left">- la taille du kernel est plus petite</td>
<td class="org-left">- moins rapide car il est divisé en deux composant</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">- plus aisé d&rsquo;ajouter un nouveau service</td>
<td class="org-left">- plus de code</td>
</tr>


<tr>
<td class="org-left">&#xa0;</td>
<td class="org-left">- plus fiable grâce à sa séparation des services</td>
<td class="org-left">&#xa0;</td>
</tr>
</tbody>
</table>



## Monolithique

Ce type de kernel est sans aucun doute le plus rapide, cela s’explique par le fait que dans ce modèle, l’espace utilisateur et l’espace kernel tournent dans la même partition virtuelle. L’espace utilisateur représente ici l’espace mémoire ou vas tourner les applications et logiciels que va lancer l’utilisateur. Tandis que l’espace kernel est un espace protégé où l’on a accès à toutes les ressources matérielles de l’ordinateur, autrement dit le hardware. Ce modèle de kernel requiert moins de code, parce qu'il n'est pas nécessaire de coder une communication entre ces deux espaces, car ils sont au même endroit dans notre partition virtuelle. Les programmes comme les pilotes par exemple tournent dans l’espace kernel. En revanche, la taille du kernel est très grande, car elle englobe l’espace kernel ainsi que des tâches qui sont souvent de l’ordre de l’espace utilisateur comme par exemple le système de fichier. Cette conception souffre d’un grand point faible : à cause de son manque de séparation une fonctionnalité ou un service peut avoir des conséquences désastreuses sur le reste du système.


## Microkernel

La taille du kernel ici est nettement plus petite que le modèle monolithique, en effet, on ne va pas se préoccuper de l’espace utilisateur autant que dans le système monolithique, on se concentre alors sur la partie du kernel, et la taille de ce kernel se trouve réduit. Seulement les tâches les plus importantes sont exécutées dans le kernel, mais les autres tâches comme le système de fichiers et les pilotes vont tourner dans l’espace utilisateur. De plus, il est plus facile de venir ajouter un service sur un modèle microkernel, car on peut tout simplement venir ajouter un nouveau service dans l’espace utilisateur sans qu’il y ait une répercussion sur l’espace kernel. Grâce à ce manque de répercussion, ce modèle est généralement plus fiable et plus facilement portable que les autres. En revanche, il faut développer plus de code pour ce cas de figure, car la séparation des espaces requiert de coder une passerelle pour communiquer entre ces deux-là. Cette séparation et ses avantages ont un prix à payer, cela concerne surtout la vitesse, car contrairement à un modèle monolithique, beaucoup d’appels système seront fait entre ces deux espaces lors de l’exécution de chaque service et cela va ralentir son utilisation.


## Hybride

Je n’ai pas pu le placer sur le diagramme, car il est en réalité très similaire au modèle microkernel, en revanche, il est un peu plus rapide. Pour accomplir cela, ce modèle décide de lancer certaines tâches qui seraient normalement de l’ordre de l’espace utilisateur dans l’espace kernel pour aller plus vite. Ce modèle est plus stable qu’un kernel monolithique, mais pas autant qu’un microkernel, il faut voir ça comme un terrain d’entente entre les deux modèles.

## Nanokernels

Ce modèle de kernel n’a pas beaucoup de documentation, car très peu répandue. Sa conception consiste à déléguer un maximum de tâches à des pilotes externe pour que le code du kernel soit le plus petit possible.

## Sources
- [Difference between microkernel and monolithic kernel](https://techdifferences.com/difference-between-microkernel-and-monolithic-kernel.html)
- [What is a kernel adress space](https://stackoverflow.com/questions/52340014/what-is-the-kernel-address-space)
- [Introduction to kernel space and user space](https://www.embhack.com/introduction-to-kernel-space-and-user-space/)
- [What is a hybrid kernel ?](https://www.techopedia.com/definition/27004/hybrid-kernel)
- [What is a nano kernel ?](https://www.techopedia.com/definition/27005/nano-kernel)
