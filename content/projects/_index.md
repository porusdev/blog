+++
aliases = ["posts", "articles", "blog", "showcase", "docs"]
title = "Projets"
author = "Raj Porus Hiruthayaraj"
tags = ["index"]
+++

Dans cette rubrique vous pouvez retrouver les projets que j'ai pu faire par moi même lors de mon temps libre ou bien des projets fait dans le cadre
de mon BTS SIO option SLAM.
