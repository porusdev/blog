+++
title = "AC19"
author = "Raj Porus Hiruthayaraj"
tags = ["projets"]
weight = 500
+++

Ce nom signifie "Aide Covid 19".

## Origine du projet

Ce projet s'est déroulé lors de ma deuxième année de BTS SIO, ce projet a été voté par des élèves de la classe.

## Le but du projet

Le projet dans sa finalité à pour but de proposer une aide aux particuliers lors de cette période difficile du COVID.
Dans l'application on peut soit être un particulier, soit un coursier, un magasin partenaire et enfin un coordinateur.
Le particulier passe une commande dans un magasin partenaire, et le coursier le lui rapporte, le rôle du coordinateur et de confier cette mission au coursier et s'assurer du bon déroulement de la livraison.

## Le stack

La partie web de ce projet est en Joomla/PHP.
Le client mobile est écrit en Java.

## A1.1.1 Analyse du cachier des charges

{{< picture "ac19figma.png" "ac19figma.png" "Image alt text" >}}

## A1.4.1 Participation à un projet 

{{< picture "ac19projWeb.png" "ac19projWeb.png" "Image alt text" >}}

{{< picture "ac19projAndroid.png" "ac19projAndroid.png" "Image alt text" >}}

