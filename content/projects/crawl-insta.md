+++
title = "Crawl Insta"
author = "Raj Porus Hiruthayaraj"
tags = ["projets"]
weight = 500
+++

Ce projet est un "crawler" non-officiel écris en Python et utilisant la librairie Selenium WebDriver.

## Que fait-il ?

Ce bot a pour fonction de parcourir les suggestions Instagram d'un compte et d'exécuter 3 types de tâches, aimer, suivre, et commenter.

## Usage 

Le bot a besoin pour cela d'une page initiale, cela est donné au bot en passant l'argument "-u" suivi du nom du compte initial, le bot pourra accéder à des profils similaires par la suite.

- Avec l'argument "-u" on passe le nom d'utilisateur du compte initial.
- Avec l'argument "-e" on passe l'adresse mail du compte associé au bot.
- Avec l'argument "-p" on passe le mot de passe du compte associé au bot.
- La présence de l'argument "-f" permet au bot de suivre les comptes qui rencontre.
- La présence de l'argument "-l" permet au bot d'aimer les posts qu'il rencontre
- Avec l'argument "-n" on défini le nombre de posts par compte rencontré sur lesquelles il pourra interagir (aimer/commenter).
- Avec l'argument "-b" on choisi le navigateur utilisé par le bot.
- L'argument "-gp" indique le chemin du Gecko driver (moteur de navigateur de Firefox)
- L'argument "-h" ou "--help" nous explique toutes les commandes, pour l'heure c'est disponible en anglais seulement.

```
$ python crawlInsta.py -h
usage: crawlInsta.py [-h] [-e E] [-p P] [-f] [-l] [-c C] [-n N] [-u U] [-b B] [-gp GP]

Unofficial Instagram crawler bot written in Python, using Selenium WebDriver

optional arguments:
  -h, --help  show this help message and exit
  -e E        The email you used for you Instagram account.
  -p P        The password you used for you Instagram account.
  -f          This flag enables follow, by default off.
  -l          This flag enables likes, by default on.
  -c C        This enables comments, by default off, you need to give it a String.
  -n N        Number of post to interact with in an account, by default set to 1
  -u U        User from what the crawling should start.
  -b B        There are two supported drivers : 'chrome','geckodriver', Chrome is set by default.
  -gp GP      Geckodriver's path

```

## Exemple
```
 python crawlInsta.py -e "your@email.com" -p "yourPassword" -f -l -n 3 -c "Hi there I am a bot !" -u "github"
```

Ici on dit au bot les choses suivantes :
- le mail du compte Instagram du bot est : your@email.com
- le mot de passe du compte Instagram du bot est : yourPassword
- le bot a l'autorisation de suivre les comptes qu'il a rencontré
- le bot a l'autorisation de aimer les posts
- le bot a l'autorisation de commenter "Hi there I am a bot !"
- le bot ne peut aimer et commenter que sur 3 posts par compte rencontré
- le bot commence son parcours sur le page Instagram de "github"


## A2.2.1 Suivi et résolution d'incidents 
C'était très difficile de faire en sorte que le bot trouve le champ correspondant à "username", j'ai du rechercher comment fonctionne les "xpath" 
pour que le bot puisse trouver ce dernier :
```
mail = self.browser.find_element_by_xpath("//input[@name='username']")
```


## Code source

N'hésitez pas à laisser des remarques sous la forme d'issues, jouer avec le bot et bien sûr le "forker" à volonté !

- [Répertoire github de crawl-insta](https://github.com/RajPorus19/crawl-Insta)
