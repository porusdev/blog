+++
title = "Sephora Lips"
author = "Raj Porus Hiruthayaraj"
tags = ["projets"]
weight = 500
+++

Ce projet a été conçu par un groupe de BTS MCO, pour lequel on a réalisé ce projet.
J'ai eu le plaisir de travailler avec Antonin Juquel durant ce projet.

## A.1.1.1 Analyse du cahier des charges d'un service à produire

J'avais réalisé cette maquette sur Figma en me basant sur le cahier des charges du groupe MCO :

{{< picture "sephora-figma.png" "sephora-figma.png" "figma" >}}

## A1.4.1 Participation à un projet 
J'avais codé le modèle utilisateur de cette application :

```
import React from 'react';

const user = {
    firstName: '',
    lastName: '',
    social: '',
    imgUrl: '',
    fidelity: '',
    admin: ''
}

const userContext = React.createContext(user);
const dispatchUserContext = React.createContext(undefined);
export const UserProvider = ({ children }) => {
    const [state, dispatch] = React.useReducer(
        (state, newValue) => ({ ...state, ...newValue }),
        user
    );
    return (
        <userContext.Provider value={state}>
            <dispatchUserContext.Provider value={dispatch}>
                {children}
            </dispatchUserContext.Provider>
        </userContext.Provider>
    );
};
export const userState = () => [
    React.useContext(userContext),
    React.useContext(dispatchUserContext)
];
```
